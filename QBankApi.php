<?php

namespace QBNK\QBank\API;

use DateTime;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use kamermans\OAuth2\GrantType\PasswordCredentials;
use kamermans\OAuth2\GrantType\RefreshToken;
use kamermans\OAuth2\OAuth2Middleware;
use kamermans\OAuth2\Persistence\SimpleCacheTokenPersistence;
use kamermans\OAuth2\Token\RawToken;
use Psr\Cache\CacheItemPoolInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Psr\SimpleCache\CacheInterface;
use QBNK\QBank\API\Controller\AccountsController;
use QBNK\QBank\API\Controller\CategoriesController;
use QBNK\QBank\API\Controller\ControllerAbstract;
use QBNK\QBank\API\Controller\DeploymentController;
use QBNK\QBank\API\Controller\EventsController;
use QBNK\QBank\API\Controller\FiltersController;
use QBNK\QBank\API\Controller\FoldersController;
use QBNK\QBank\API\Controller\MediaController;
use QBNK\QBank\API\Controller\MoodboardsController;
use QBNK\QBank\API\Controller\ObjecttypesController;
use QBNK\QBank\API\Controller\PropertysetsController;
use QBNK\QBank\API\Controller\SearchController;
use QBNK\QBank\API\Controller\SocialmediaController;
use QBNK\QBank\API\Controller\TemplatesController;
use QBNK\QBank\API\Controller\WebhooksController;
use Symfony\Component\Cache\Adapter\ArrayAdapter;
use Symfony\Component\Cache\Adapter\Psr16Adapter;
use Symfony\Component\Cache\Psr16Cache;

/**
 * This is the main class to instantiate and use when communicating with QBank.
 *
 * All the different parts of the API are accessible via the methods which are returning controllers.
 *
 * Built against QBank API v.1 and Swagger v.1.1
 */
class QBankApi
{
    public const MAJOR_VERSION = 6;


    /** @var LoggerInterface */
    protected $logger;

    /** @var string */
    protected $basepath;

    /** @var string */
    protected $namespace;

    /** @var Credentials */
    protected $credentials;

    /** @var CacheInterface */
    protected $tokenCache;

    /** @var CacheItemPoolInterface */
    protected $apiCache;

    /** @var CachePolicy */
    protected $cachePolicy;

    /** @var Client */
    protected $client;

    /** @var OAuth2Middleware */
    protected $oauth2Middleware;

    /** @var bool */
    protected $verifyCertificates;

    /** @var ControllerAbstract */
    protected $controllers = [];

    /** @var array */
    public static $extraCacheHashValues = [];

    /**
     * @param string      $qbankURL    the URL to the QBank API
     * @param Credentials $credentials the credentials used to connect
     * @param array       $options     Associative array containing options.
     *                                 <ul>
     *                                 <li>CacheInterface $options[cache] A cache implementation to store tokens and responses in. Highly recommended.</li>
     *                                 <li>QBankCachePolicy $options[cachePolicy] A policy on how to use caching for API queries, if not provided cache will not be available for API queries.</li>
     *                                 <li>LoggerInterface $options[log] A PSR-3 log implementation.</li>
     *                                 <li>bool $options[verifyCertificates] Whether to verify certificates for https connections. Defaults to true.</li>
     *                                 </ul>
     *
     * @throws \LogicException
     */
    public function __construct($qbankURL, Credentials $credentials, array $options = [])
    {
        // Setup logging
        if (!empty($options['log']) && $options['log'] instanceof LoggerInterface) {
            $this->logger = $options['log'];
        } else {
            $this->logger = new NullLogger();
        }

        $this->basepath = $this->buildBasepath($qbankURL);

        // Store credentials for later use
        $this->credentials = $credentials;

        if (!empty($options['namespace'])) {
            $this->namespace = $options['namespace'];
        } else {
            $this->namespace = md5($this->basepath . $this->credentials->getUsername() . $this->credentials->getPassword());
        }

        $cache = $options['cache'] ?? null;
        switch (true) {
            case $cache instanceof CacheItemPoolInterface:
                $this->tokenCache = new Psr16Cache($cache);
                $this->apiCache = $cache;
                break;
            case $cache instanceof CacheInterface:
                $this->tokenCache = $cache;
                $this->apiCache = new Psr16Adapter($cache);
                break;
            default:
                $this->tokenCache = new Psr16Cache(new ArrayAdapter(storeSerialized: false));
                $this->apiCache = new ArrayAdapter(storeSerialized: false);
                $this->logger->notice('No caching supplied! Using build-in array cache instead.');
        }

        // Setup the cache policy
        if (!empty($options['cachePolicy']) && $options['cachePolicy'] instanceof CachePolicy) {
            $this->cachePolicy = $options['cachePolicy'];
            if (empty($options['cache']) && $this->cachePolicy->isEnabled()) {
                throw new \LogicException(
                    'You have supplied a cache policy that says cache is enabled but no cache provider have been defined.'
                );
            }
        } else {
            $this->cachePolicy = new CachePolicy(false, 0);
            $this->logger->warning('No cache policy supplied! Without a cache policy no API queries will be cached.');
        }

        if (isset($options['verifyCertificates'])) {
            $this->verifyCertificates = (bool) $options['verifyCertificates'];
        } else {
            $this->verifyCertificates = true;
        }
    }


    /**
     * Accounts control the security in QBank.
     *
     * @return AccountsController
     */
    public function accounts()
    {
        if (empty($this->controllers['accounts'])) {
            $this->controllers['accounts'] = new AccountsController($this->getClient(), $this->cachePolicy, $this->apiCache);
            $this->controllers['accounts']->setLogger($this->logger);
        }

        return $this->controllers['accounts'];
    }

    /**
     * Categories defines which PropertySets should be available for Media. All Media belongs to exactly one Category.
     *
     * @return CategoriesController
     */
    public function categories()
    {
        if (empty($this->controllers['categories'])) {
            $this->controllers['categories'] = new CategoriesController($this->getClient(), $this->cachePolicy, $this->apiCache);
            $this->controllers['categories']->setLogger($this->logger);
        }

        return $this->controllers['categories'];
    }

    /**
     * DeploymentSites are places where Media from QBank may be published to, to allow public access. Protocols define the way the publishing executes.
     *
     * @return DeploymentController
     */
    public function deployment()
    {
        if (empty($this->controllers['deployment'])) {
            $this->controllers['deployment'] = new DeploymentController($this->getClient(), $this->cachePolicy, $this->apiCache);
            $this->controllers['deployment']->setLogger($this->logger);
        }

        return $this->controllers['deployment'];
    }

    /**
     * Class Events.
     *
     * @return EventsController
     */
    public function events()
    {
        if (empty($this->controllers['events'])) {
            $this->controllers['events'] = new EventsController($this->getClient(), $this->cachePolicy, $this->apiCache);
            $this->controllers['events']->setLogger($this->logger);
        }

        return $this->controllers['events'];
    }

    /**
     * Filters are used for filtering media by its folder, category or a specific property.
     *
     * @return FiltersController
     */
    public function filters()
    {
        if (empty($this->controllers['filters'])) {
            $this->controllers['filters'] = new FiltersController($this->getClient(), $this->cachePolicy, $this->apiCache);
            $this->controllers['filters']->setLogger($this->logger);
        }

        return $this->controllers['filters'];
    }

    /**
     * Folders are used to group Media in a hierarchial manner.
     *
     * @return FoldersController
     */
    public function folders()
    {
        if (empty($this->controllers['folders'])) {
            $this->controllers['folders'] = new FoldersController($this->getClient(), $this->cachePolicy, $this->apiCache);
            $this->controllers['folders']->setLogger($this->logger);
        }

        return $this->controllers['folders'];
    }

    /**
     * A Media is any uploaded file in QBank. A Media belongs to a Category and may have customer defined Properties.
     *
     * @return MediaController
     */
    public function media()
    {
        if (empty($this->controllers['media'])) {
            $this->controllers['media'] = new MediaController($this->getClient(), $this->cachePolicy, $this->apiCache);
            $this->controllers['media']->setLogger($this->logger);
        }

        return $this->controllers['media'];
    }

    /**
     * Moodboards are public, usually temporary, areas used to expose Media in QBank. Any Media may be added to a Moodboard, which any outside user may then access until the Moodboard expiration date is due. Moodboards can be templated in different ways to fit many purposes.
     *
     * @return MoodboardsController
     */
    public function moodboards()
    {
        if (empty($this->controllers['moodboards'])) {
            $this->controllers['moodboards'] = new MoodboardsController($this->getClient(), $this->cachePolicy, $this->apiCache);
            $this->controllers['moodboards']->setLogger($this->logger);
        }

        return $this->controllers['moodboards'];
    }

    /**
     * Object types define sets of propertySets that can be applied to any Object of the corresponding object type class, such as a Media or a Folder.
     *
     * @return ObjecttypesController
     */
    public function objecttypes()
    {
        if (empty($this->controllers['objecttypes'])) {
            $this->controllers['objecttypes'] = new ObjecttypesController($this->getClient(), $this->cachePolicy, $this->apiCache);
            $this->controllers['objecttypes']->setLogger($this->logger);
        }

        return $this->controllers['objecttypes'];
    }

    /**
     * PropertySets groups Properties together.
     *
     * @return PropertysetsController
     */
    public function propertysets()
    {
        if (empty($this->controllers['propertysets'])) {
            $this->controllers['propertysets'] = new PropertysetsController($this->getClient(), $this->cachePolicy, $this->apiCache);
            $this->controllers['propertysets']->setLogger($this->logger);
        }

        return $this->controllers['propertysets'];
    }

    /**
     * @return SearchController
     */
    public function search()
    {
        if (empty($this->controllers['search'])) {
            $this->controllers['search'] = new SearchController($this->getClient(), $this->cachePolicy, $this->apiCache);
            $this->controllers['search']->setLogger($this->logger);
        }

        return $this->controllers['search'];
    }

    /**
     * SocialMedia are places where Media from QBank may be published to, to allow public access. Protocols define the way the publishing executes.
     *
     * @return SocialmediaController
     */
    public function socialmedia()
    {
        if (empty($this->controllers['socialmedia'])) {
            $this->controllers['socialmedia'] = new SocialmediaController($this->getClient(), $this->cachePolicy, $this->apiCache);
            $this->controllers['socialmedia']->setLogger($this->logger);
        }

        return $this->controllers['socialmedia'];
    }

    /**
     * @return TemplatesController
     */
    public function templates()
    {
        if (empty($this->controllers['templates'])) {
            $this->controllers['templates'] = new TemplatesController($this->getClient(), $this->cachePolicy, $this->apiCache);
            $this->controllers['templates']->setLogger($this->logger);
        }

        return $this->controllers['templates'];
    }

    /**
     * Connect your applications with QBank through webhooks.
     *
     * @return WebhooksController
     */
    public function webhooks()
    {
        if (empty($this->controllers['webhooks'])) {
            $this->controllers['webhooks'] = new WebhooksController($this->getClient(), $this->cachePolicy, $this->apiCache);
            $this->controllers['webhooks']->setLogger($this->logger);
        }

        return $this->controllers['webhooks'];
    }

    /**
     * Create a basepath for all api calls from the supplied URL.
     *
     * @param string $url
     *
     * @return string
     */
    protected function buildBasepath($url)
    {
        if (!preg_match('#(\w+:)?//#', $url)) {
            $url = '//' . $url;
        }

        $urlParts = parse_url($url);

        if (false === $urlParts) {
            throw new \InvalidArgumentException('Could not parse QBank URL.');
        }

        // Default to HTTPS
        if (empty($urlParts['scheme'])) {
            $urlParts['scheme'] = 'https';
        }

        // Add the api path automatically if omitted for qbank.se hosted QBank instances
        if (
            (empty($urlParts['path']) || '/' === $urlParts['path'])
            && 'qbank.se' === substr($urlParts['host'], -strlen('qbank.se'))
        ) {
            $urlParts['path'] = '/api/';
        }

        // Pad the end of the path with a slash
        if ('/' !== substr($urlParts['path'], -1)) {
            $urlParts['path'] .= '/';
        }

        return $urlParts['scheme'] . '://' . $urlParts['host'] . (!empty($urlParts['port']) ? ':' . $urlParts['port'] : '') . $urlParts['path'];
    }

    /**
     * Gets the Guzzle client instance used for making calls.
     *
     * @return Client
     */
    protected function getClient()
    {
        if (!($this->client instanceof Client)) {
            $handlerStack = HandlerStack::create();
            $handlerStack = $this->withOAuth2MiddleWare($handlerStack);
            $this->client = new Client([
                'handler' => $handlerStack,
                'auth' => 'oauth',
                'base_uri' => $this->basepath,
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-type' => 'application/json',
                    'User-Agent' => 'qbank3api-phpwrapper/' . self::MAJOR_VERSION . ' (qbankapi: 1; swagger: 1.1)',
                ],
                'verify' => $this->verifyCertificates,
                'allow_redirects' => [
                    'max' => 5,
                    'strict' => false,
                    'referer' => false,
                    'protocols' => ['http', 'https'],
                    'track_redirects' => false,
                ],
            ]);

            $this->logger->debug('Guzzle client instantiated.', ['basepath' => $this->basepath]);
        }

        return $this->client;
    }

    protected function updateConsumerClients()
    {
        foreach ($this->controllers as $controller) {
            if ($controller instanceof ControllerAbstract && $controller->getClient() instanceof Client) {
                $controller->setClient($this->client);
            }
        }
    }

    /**
     * Adds the OAuth2 middleware to the handler stack.
     *
     * @return HandlerStack
     */
    protected function withOAuth2MiddleWare(HandlerStack $stack)
    {
        if (!($this->oauth2Middleware instanceof OAuth2Middleware)) {
            $this->oauth2Middleware = $this->getOAuthMiddleware();
            $this->oauth2Middleware->setTokenPersistence(new SimpleCacheTokenPersistence($this->tokenCache, $this->namespace . '_token'));
        }

        $stack->push($this->oauth2Middleware);

        return $stack;
    }

    /**
     * Configures the OAUth middleware.
     */
    protected function getOAuthMiddleware(): OAuth2Middleware
    {
        $oauthClient = new Client([
            'base_uri' => $this->basepath . 'oauth2/token',
            'verify' => $this->verifyCertificates,
            'headers' => [
                'User-Agent' => 'qbank3api-phpwrapper/ ' . self::MAJOR_VERSION . ' (qbankapi: 1; swagger: 1.1)',
            ],
        ]);
        $config = [
            'username' => $this->credentials->getUsername(),
            'password' => $this->credentials->getPassword(),
            'client_id' => $this->credentials->getClientId(),
        ];
        return new OAuth2Middleware(
            new PasswordCredentials($oauthClient, $config),
            new RefreshToken($oauthClient, $config)
        );
    }

    /**
     * Changes the credentials used to authenticate with QBank.
     *
     * Changing the credentials will effectively switch the user using QBank and is useful when implementing some tiered
     * service.
     *
     * @param string $user     username of the new user
     * @param string $password password of the new user
     */
    public function updateCredentials($user, $password)
    {
        $oldUser = $this->credentials->getUsername();
        $this->credentials = new Credentials($this->credentials->getClientId(), $user, $password);
        $this->namespace = md5($this->basepath . $user . $password);
        unset($password);
        if ($this->client instanceof Client) {
            $this->client = $this->oauth2Middleware = null;
            $this->client = $this->getClient();
            $this->updateConsumerClients();
        }

        $this->logger->notice('Updated user!', ['old' => $oldUser, 'new' => $user]);
    }

    /**
     * Sets the tokens used for authentication.
     *
     * This is normally done automatically, but exposed for transparency reasons.
     */
    public function setTokens(string $accessToken, string $refreshToken = null, int|DateTime $expiresAt = null): void
    {
        if (!($this->oauth2Middleware instanceof OAuth2Middleware)) {
            $this->oauth2Middleware = $this->getOAuthMiddleware();
        }

        if ($expiresAt instanceof DateTime) {
            $expiresAt = $expiresAt->getTimestamp();
        }

        $this->oauth2Middleware->setAccessToken(new RawToken($accessToken, $refreshToken, $expiresAt));
    }

    /**
     * Gets the token used for authentication.
     *
     * @return array ['accessToken' => string|null, 'refreshToken' => string|null]
     */
    public function getTokens(): array
    {
        if ($this->oauth2Middleware instanceof OAuth2Middleware) {
            $tokens['accessToken'] = $this->oauth2Middleware->getAccessToken();
            $tokens['refreshToken'] = $this->oauth2Middleware->getRawToken()->getRefreshToken();
        }

        if (empty($tokens['accessToken'])) {
            try {
                $this->getClient()->get('/');      // Trigger call to get a token. Don't care about the result.
            } catch (\Throwable) {}
            $tokens['accessToken'] = $this->oauth2Middleware->getAccessToken();
            $tokens['refreshToken'] = $this->oauth2Middleware->getRawToken()->getRefreshToken();
        }

        return $tokens;
    }

    /**
     * Get the apiCache, in case the user want access to the namespaced cache
     */
    public function getApiCache()
    {
        return $this->apiCache;
    }
}
