<?php

namespace QBNK\QBank\API\Test\Unit\Controller;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Mockery;
use PHPUnit\Framework\TestCase;
use Psr\Log\NullLogger;
use QBNK\QBank\API\CachePolicy;
use QBNK\QBank\API\Controller\MediaController;

final class MediaControllerTest extends TestCase
{
    public function testRetrieveMedia(): void
    {
        $parameters = [
            'query' => ['includeChildren' => false],
            'headers' => [],
        ];
        $response = new Response(
            200,
            ['Content-type' => 'application/json'],
            file_get_contents(__DIR__ . '/responses/retrieve-media.json')
        );
        /** @var Client|Mockery\MockInterface $client */
        $client = Mockery::mock(Client::class)->makePartial();
        $client
            ->shouldReceive('get')
            ->with('v1/media/1337', $parameters)
            ->andReturn($response)
        ;
        $cachePolicy = new CachePolicy(CachePolicy::OFF, 0);
        $mediaController = new MediaController($client, $cachePolicy);
        $mediaController->setLogger(new NullLogger());
        $media = $mediaController->retrieveMedia(1337);
        $this->assertEquals(1337, $media->getMediaId());
        $this->assertNotEmpty($media->getPropertySets());
        $this->assertContains('Logotype', $media->getProperty('keywords')->getValue());
    }
}
