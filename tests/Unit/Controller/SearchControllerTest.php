<?php

namespace QBNK\QBank\API\Test\Unit\Controller;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Mockery;
use Psr\Log\NullLogger;
use QBNK\QBank\API\CachePolicy;
use QBNK\QBank\API\Controller\SearchController;
use PHPUnit\Framework\TestCase;
use QBNK\QBank\API\Model\MediaResponse;
use QBNK\QBank\API\Model\Search;

final class SearchControllerTest extends TestCase
{

    public function testSearch()
    {
        $search = new Search([
            'mediaIds' => [1337]
        ]);
        $parameters = [
            'query' => ['returnType' => SearchController::RETURN_OBJECTS],
            'body' => json_encode(['search' => $search], JSON_UNESCAPED_UNICODE),
            'headers' => [],
        ];
        $response = new Response(
            200,
            ['Content-type' => 'application/json'],
            file_get_contents(__DIR__ . '/responses/search.json')
        );
        /** @var Client|Mockery\MockInterface $client */
        $client = Mockery::mock(Client::class)->makePartial();
        $client
            ->shouldReceive('post')
            ->with('v1/search', $parameters)
            ->andReturn($response)
        ;
        $cachePolicy = new CachePolicy(CachePolicy::OFF, 0);
        $searchController = new SearchController($client, $cachePolicy);
        $searchController->setLogger(new NullLogger());
        $result = $searchController->search($search);
        $this->assertEquals(1, $result->getTotalHits());
        $this->assertNotEmpty($result->getResults());
        foreach ($result as $media) {
            $this->assertInstanceOf(MediaResponse::class, $media);
        }
    }
}
