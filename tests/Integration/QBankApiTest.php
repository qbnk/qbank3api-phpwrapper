<?php

namespace QBNK\QBank\API\Test\Integration;

use QBNK\QBank\API\CachePolicy;
use QBNK\QBank\API\Credentials;
use QBNK\QBank\API\QBankApi;
use PHPUnit\Framework\TestCase;
use Psr\Cache\CacheItemPoolInterface;
use RuntimeException;
use Symfony\Component\Cache\Adapter\ArrayAdapter;

final class QBankApiTest extends TestCase
{
    private Credentials $credentials;
    private CacheItemPoolInterface $cache;

    public function setUp(): void
    {
        parent::setUp();
        if (empty(getenv('QBNK_API_CLIENTID'))
            || empty(getenv('QBNK_API_USERNAME'))
            || empty(getenv('QBNK_API_PASSWORD')))
        {
            throw new RuntimeException('Missing credentials for the QBank API! Must be set as environment variables.');
        }
        $this->credentials = new Credentials(
            getenv('QBNK_API_CLIENTID'),
            getenv('QBNK_API_USERNAME'),
            getenv('QBNK_API_PASSWORD')
        );

        $this->cache = new ArrayAdapter();
    }

    public function testGetTokens()
    {
        $api = new QBankApi(
            'v3.qbank.se',
            $this->credentials,
            [
                'cache' => $this->cache,
                'cachePolicy' => new CachePolicy(CachePolicy::TOKEN_ONLY, 300)
            ]
        );
        $tokens = $api->getTokens();
        $this->assertArrayHasKey('accessToken', $tokens);
        $this->assertNotNull($tokens['accessToken']);
        $this->assertIsString($tokens['accessToken']);
        $this->assertNotEmpty($tokens['accessToken']);
        $this->assertArrayHasKey('refreshToken', $tokens);
        $this->assertNotNull($tokens['refreshToken']);
        $this->assertIsString($tokens['refreshToken']);
        $this->assertNotEmpty($tokens['refreshToken']);
    }
}
