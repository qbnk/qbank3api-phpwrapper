# Upgrading
This is a guide on how to upgrade between major versions.

## Version 4 to 5
The OAuth2 library has been replaced and due to that the exposed methods dealing with tokens have changed. If you handle 
tokens manually, review that to make sure that the types of parameters and return values are correct.

The caching component has changed. It is now just the PSR-16 (Simple Cache) interface, so you are free to use whatever 
caching library you like that implements that.