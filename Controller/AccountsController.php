<?php

namespace QBNK\QBank\API\Controller;

use QBNK\QBank\API\CachePolicy;
use QBNK\QBank\API\Model\ClientId;
use QBNK\QBank\API\Model\Functionality;
use QBNK\QBank\API\Model\Group;
use QBNK\QBank\API\Model\Role;
use QBNK\QBank\API\Model\User;

class AccountsController extends ControllerAbstract
{
    /**
     * Add a ClientId.
     *
     * @return ClientId
     */
    public function addClientId(ClientId $clientId)
    {
        $parameters = [
            'query' => [],
            'body' => json_encode(['clientId' => $clientId], JSON_UNESCAPED_UNICODE),
            'headers' => [],
        ];

        $result = $this->post('v1/accounts/clientid', $parameters);
        return new ClientId($result);
    }

    /**
     * Removes an existing ClientId.
     *
     * @param string $id The identifier of the clientId
     */
    public function deleteClientId($id)
    {
        $parameters = [
            'query' => [],
            'headers' => [],
        ];

        return $this->delete('v1/accounts/clientid/'.$id, $parameters);
    }


    /**
     * Lists all ClientIds available.
     *
     * @param CachePolicy $cachePolicy    a custom cache policy used for this request only
     *
     * @return ClientId[]
     */
    public function listClientIds(CachePolicy $cachePolicy = null)
    {
        $parameters = [
            'headers' => [],
        ];

        $result = $this->get('v1/accounts/clientids', $parameters, $cachePolicy);
        foreach ($result as &$entry) {
            $entry = new ClientId($entry);
        }
        unset($entry);
        reset($result);

        return $result;
    }

    /**
     * Gets a ClientId.
     *
     * Fetches a ClientId by the specified identifier.
     *
     * @param string      $id          the ClientId identifier
     * @param CachePolicy $cachePolicy a custom cache policy used for this request only
     *
     * @return ClientId
     */
    public function retrieveClientId($id, CachePolicy $cachePolicy = null)
    {
        $parameters = [
            'query' => [],
            'headers' => [],
        ];

        $result = $this->get('v1/accounts/clientid/' . $id . '', $parameters, $cachePolicy);
        return new ClientId($result);
    }

    /**
     * Updates a ClientId.
     *
     * Updates a previously created ClientId.
     *
     * @param ClientId $clientId The ClientId object.
     *
     * @return ClientId
     */
    public function updateClientId(ClientId $clientId)
    {
        $parameters = [
            'body' => json_encode(['clientId' => $clientId], JSON_UNESCAPED_UNICODE),
            'headers' => [],
        ];

        $result = $this->put('v1/accounts/clientid', $parameters);
        return new ClientId($result);
    }

    /**
     * Lists Functionalities available.
     *
     * Lists all Functionalities available
     *
     * @param bool        $includeDeleted indicates if we should include removed Functionalities in the result
     * @param CachePolicy $cachePolicy    a custom cache policy used for this request only
     *
     * @return Functionality[]
     */
    public function listFunctionalities($includeDeleted = false, CachePolicy $cachePolicy = null)
    {
        $parameters = [
            'query' => ['includeDeleted' => $includeDeleted],
            'headers' => [],
        ];

        $result = $this->get('v1/accounts/functionalities', $parameters, $cachePolicy);
        foreach ($result as &$entry) {
            $entry = new Functionality($entry);
        }
        unset($entry);
        reset($result);

        return $result;
    }

    /**
     * Fetches a specific Functionality.
     *
     * Fetches a Functionality by the specified identifier.
     *
     * @param int         $id          the Functionality identifier
     * @param CachePolicy $cachePolicy a custom cache policy used for this request only
     *
     * @return Functionality
     */
    public function retrieveFunctionality($id, CachePolicy $cachePolicy = null)
    {
        $parameters = [
            'query' => [],
            'headers' => [],
        ];

        $result = $this->get('v1/accounts/functionalities/' . $id . '', $parameters, $cachePolicy);
        $result = new Functionality($result);

        return $result;
    }

    /**
     * Lists Groups available.
     *
     * Lists all Groups available
     *
     * @param bool        $includeDeleted indicates if we should include removed Groups in the result
     * @param CachePolicy $cachePolicy    a custom cache policy used for this request only
     *
     * @return Group[]
     */
    public function listGroups($includeDeleted = false, CachePolicy $cachePolicy = null)
    {
        $parameters = [
            'query' => ['includeDeleted' => $includeDeleted],
            'headers' => [],
        ];

        $result = $this->get('v1/accounts/groups', $parameters, $cachePolicy);
        foreach ($result as &$entry) {
            $entry = new Group($entry);
        }
        unset($entry);
        reset($result);

        return $result;
    }

    /**
     * Fetches a specific Group.
     *
     * Fetches a Group by the specified identifier.
     *
     * @param int         $id           the Group identifier
     * @param bool        $includeUsers
     * @param CachePolicy $cachePolicy  a custom cache policy used for this request only
     *
     * @return Group
     */
    public function retrieveGroup($id, $includeUsers = false, CachePolicy $cachePolicy = null)
    {
        $parameters = [
            'query' => ['includeUsers' => $includeUsers],
            'headers' => [],
        ];

        $result = $this->get('v1/accounts/groups/' . $id . '', $parameters, $cachePolicy);
        $result = new Group($result);

        return $result;
    }

    /**
     * Fetches the currently logged in User.
     *
     * Effectively a whoami call.
     *
     * @param CachePolicy $cachePolicy a custom cache policy used for this request only
     *
     * @return User
     */
    public function retrieveCurrentUser(CachePolicy $cachePolicy = null)
    {
        $parameters = [
            'query' => [],
            'headers' => [],
        ];

        $result = $this->get('v1/accounts/me', $parameters, $cachePolicy);
        $result = new User($result);

        return $result;
    }

    /**
     * Lists Roles available.
     *
     * Lists all Roles available
     *
     * @param bool        $includeDeleted indicates if we should include removed Roles in the result
     * @param CachePolicy $cachePolicy    a custom cache policy used for this request only
     *
     * @return Role[]
     */
    public function listRoles($includeDeleted = false, CachePolicy $cachePolicy = null)
    {
        $parameters = [
            'query' => ['includeDeleted' => $includeDeleted],
            'headers' => [],
        ];

        $result = $this->get('v1/accounts/roles', $parameters, $cachePolicy);
        foreach ($result as &$entry) {
            $entry = new Role($entry);
        }
        unset($entry);
        reset($result);

        return $result;
    }

    /**
     * Fetches a specific Role.
     *
     * Fetches a Role by the specified identifier.
     *
     * @param int         $id          the Role identifier
     * @param CachePolicy $cachePolicy a custom cache policy used for this request only
     *
     * @return Role
     */
    public function retrieveRole($id, CachePolicy $cachePolicy = null)
    {
        $parameters = [
            'query' => [],
            'headers' => [],
        ];

        $result = $this->get('v1/accounts/roles/' . $id . '', $parameters, $cachePolicy);
        $result = new Role($result);

        return $result;
    }

    /**
     * Fetches all settings.
     *
     * Fetches all settings currently available for the current user.
     *
     * @param bool $default Whether to only get default settings.
     * @param CachePolicy|null $cachePolicy a custom cache policy used for this request only
     *
     * @return array
     */
    public function listSettings($default = false, CachePolicy $cachePolicy = null)
    {
        $parameters = [
            'query' => ['default' => $default],
            'headers' => [],
        ];

        $result = $this->get('v1/accounts/settings', $parameters, $cachePolicy);

        return $result;
    }

    /**
     * Fetches a setting.
     *
     * Fetches a setting for the current user.
     *
     * @param string $key The key of the setting to fetch.
     * @param bool $default Whether to only get default settings.
     * @param CachePolicy $cachePolicy a custom cache policy used for this request only
     *
     * @return array
     */
    public function retrieveSetting($key, $default = false, CachePolicy $cachePolicy = null)
    {
        $parameters = [
            'query' => ['default' => $default],
            'headers' => [],
        ];

        $result = $this->get('v1/accounts/settings/' . $key . '', $parameters, $cachePolicy);

        return $result;
    }

    /**
     * Lists Users available.
     *
     * Lists all Users available
     *
     * @param bool        $includeDeleted indicates if we should include removed Users in the result
     * @param CachePolicy $cachePolicy    a custom cache policy used for this request only
     *
     * @return User[]
     */
    public function listUsers($includeDeleted = false, CachePolicy $cachePolicy = null)
    {
        $parameters = [
            'query' => ['includeDeleted' => $includeDeleted],
            'headers' => [],
        ];

        $result = $this->get('v1/accounts/users', $parameters, $cachePolicy);
        foreach ($result as &$entry) {
            $entry = new User($entry);
        }
        unset($entry);
        reset($result);

        return $result;
    }

    /**
     * Fetches a specific User.
     *
     * Fetches a User by the specified identifier.
     *
     * @param int         $id          the User identifier
     * @param CachePolicy $cachePolicy a custom cache policy used for this request only
     *
     * @return User
     */
    public function retrieveUser($id, CachePolicy $cachePolicy = null)
    {
        $parameters = [
            'query' => [],
            'headers' => [],
        ];

        $result = $this->get('v1/accounts/users/' . $id . '', $parameters, $cachePolicy);
        $result = new User($result);

        return $result;
    }

    /**
     * Search for users by email, group or user type.
     *
     * @param ?string      $byEmail     search for users by email
     * @param ?string      $userType    which user types to consider
     * @param ?CachePolicy $cachePolicy a custom cache policy used for this request only
     * @param ?string     $groupId    which group to filter on
     * @param ?int        $offset    offset of the search
     * @param ?int        $limit    limit the number of users returned
     *
     * @return User[]
     */
    public function searchUsers($byEmail = '', $userType = 'all_users', CachePolicy $cachePolicy = null, $groupId = '', $offset = 0, $limit = 30)
    {
        $parameters = [
            'query' => ['byEmail' => $byEmail, 'userType' => $userType, 'groupId' => $groupId, 'offset' => $offset, 'limit' => $limit],
            'headers' => [],
        ];

        $result = $this->get('v1/accounts/users/search', $parameters, $cachePolicy);
        foreach ($result as &$entry) {
            $entry = new User($entry);
        }
        unset($entry);
        reset($result);

        return $result;
    }

    /**
     * Creates a new setting.
     *
     * Creates a new, previously not existing setting.
     *
     * @param string $key   The key (identifier) of the setting
     * @param string $value The value of the setting
     * @param bool $default Whether the setting is a default setting or not.
     */
    public function createSetting($key, $value, $default = false)
    {
        $parameters = [
            'query' => ['default' => $default],
            'body' => json_encode(['key' => $key, 'value' => $value], JSON_UNESCAPED_UNICODE),
            'headers' => [],
        ];

        $result = $this->post('v1/accounts/settings', $parameters);

        return $result;
    }

    /**
     * Create a user Create a user in QBank.
     *
     * @param User   $user                  The user to create
     * @param string $password              Password for the new user, leave blank to let QBank send a password-reset link to the user
     * @param string $redirectTo            Only used if leaving $password blank, a URL to redirect the user to after setting his/hers password
     * @param bool   $sendNotificationEmail Send a notification email to the new user, as specified through the QBank backend
     *
     * @return User
     */
    public function createUser(User $user, $password = null, $redirectTo = null, $sendNotificationEmail = null)
    {
        $parameters = [
            'query' => [],
            'body' => json_encode(['user' => $user, 'password' => $password, 'redirectTo' => $redirectTo, 'sendNotificationEmail' => $sendNotificationEmail], JSON_UNESCAPED_UNICODE),
            'headers' => [],
        ];

        $result = $this->post('v1/accounts/users', $parameters);
        $result = new User($result);

        return $result;
    }

    /**
     * Update a user Update a user in QBank.
     *
     * @param int    $id
     * @param User   $user     The user to update
     * @param string $password Set a new password for the user, leave blank to leave unchanged
     *
     * @return User
     */
    public function updateUser($id, User $user, $password = null)
    {
        $parameters = [
            'query' => [],
            'body' => json_encode(['user' => $user, 'password' => $password], JSON_UNESCAPED_UNICODE),
            'headers' => [],
        ];

        $result = $this->post('v1/accounts/users/' . $id . '', $parameters);
        $result = new User($result);

        return $result;
    }

    /**
     * Add the user to one or more groups.
     *
     * @param int   $id
     * @param int[] $groupIds an array of int values
     *
     * @return User
     */
    public function addUserToGroup($id, array $groupIds)
    {
        $parameters = [
            'query' => [],
            'body' => json_encode(['groupIds' => $groupIds], JSON_UNESCAPED_UNICODE),
            'headers' => [],
        ];

        $result = $this->post('v1/accounts/users/' . $id . '/groups', $parameters);
        $result = new User($result);

        return $result;
    }

    /**
     * Update the last login time for a user Update the last login time for a user.
     *
     * @param int  $id
     * @param bool $successful Login attempt successful or not
     *
     * @return User
     */
    public function updateLastLogin($id, $successful = null)
    {
        $parameters = [
            'query' => [],
            'body' => json_encode(['successful' => $successful], JSON_UNESCAPED_UNICODE),
            'headers' => [],
        ];

        $result = $this->post('v1/accounts/users/' . $id . '/registerloginattempt', $parameters);
        $result = new User($result);

        return $result;
    }

    /**
     * Dispatch a password reset mail to a user.
     *
     * . The supplied link will be included in the mail and appended with a "hash=" parameter containing the password reset hash needed to set the new password in step 2.
     *
     * @param int    $id   the User identifier
     * @param string $link Optional link to override redirect to in the password reset mail
     */
    public function sendPasswordReset($id, $link = null)
    {
        $parameters = [
            'query' => [],
            'body' => json_encode(['link' => $link], JSON_UNESCAPED_UNICODE),
            'headers' => [],
        ];

        $result = $this->post('v1/accounts/users/' . $id . '/resetpassword', $parameters);

        return $result;
    }

    /**
     * Reset a password for a user with password reset hash.
     *
     * Resets a password for a user with a valid password reset hash. Hash should be obtained through "/users/{id}/sendpasswordreset".
     *
     * @param string $hash     Valid password reset hash
     * @param string $password New password
     *
     * @return array
     */
    public function resetPassword($hash, $password)
    {
        $parameters = [
            'query' => [],
            'body' => json_encode(['hash' => $hash, 'password' => $password], JSON_UNESCAPED_UNICODE),
            'headers' => [],
        ];

        $result = $this->post('v1/accounts/users/resetpassword', $parameters);

        return $result;
    }

    /**
     * Updates an existing setting.
     *
     * Updates a previously created setting.
     *
     * @param string $key   The key (identifier) of the setting..
     * @param string $value The value of the setting
     * @param bool $default Whether the setting is a default setting or not.
     */
    public function updateSetting($key, $value, $default = false)
    {
        $parameters = [
            'query' => ['default' => $default],
            'body' => json_encode(['value' => $value], JSON_UNESCAPED_UNICODE),
            'headers' => [],
        ];

        $result = $this->put('v1/accounts/settings/' . $key . '', $parameters);

        return $result;
    }

    /**
     * Removes an existing setting.
     *
     * Updates a previously created setting.
     *
     * @param string $key The key (identifier) of the setting..
     * @param bool $default Whether the setting is a default setting or not.
     */
    public function removeSetting($key, $default = false)
    {
        $parameters = [
            'query' => ['default' => $default],
            'headers' => [],
        ];

        $result = $this->delete('v1/accounts/settings/' . $key . '', $parameters);

        return $result;
    }

    /**
     * Removes a setting completely.
     *
     * Removes the setting for every user and any default value for it.
     *
     * @param string $key
     */
    public function removeSettingCompletely($key)
    {
        $parameters = [
            'query' => [],
            'headers' => []
        ];

        $result = $this->delete('v1/accounts/settings/' . $key . '/completely', $parameters);

        return $result;
    }

    /**
     * Remove the user from one or more groups.
     *
     * @param int    $id
     * @param string $groupIds a comma separated string of group ids we should remove the user from
     *
     * @return User
     */
    public function removeUserFromGroup($id, $groupIds)
    {
        $parameters = [
            'query' => ['groupIds' => $groupIds],
            'headers' => [],
        ];

        $result = $this->delete('v1/accounts/users/' . $id . '/groups', $parameters);
        $result = new User($result);

        return $result;
    }
}
