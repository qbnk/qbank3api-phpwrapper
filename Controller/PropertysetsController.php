<?php

namespace QBNK\QBank\API\Controller;

use QBNK\QBank\API\CachePolicy;
use QBNK\QBank\API\Model\PropertySet;
use QBNK\QBank\API\Model\PropertyType;

class PropertysetsController extends ControllerAbstract
{
    /**
     * Lists all PropertySets.
     *

     * @param CachePolicy $cachePolicy a custom cache policy used for this request only
     *
     * @return PropertySet[]
     */
    public function listPropertySets(CachePolicy $cachePolicy = null)
    {
        $parameters = [
            'query' => [],
            'headers' => [],
        ];

        $result = $this->get('v1/propertysets', $parameters, $cachePolicy);
        foreach ($result as &$entry) {
            $entry = new PropertySet($entry);
        }
        unset($entry);
        reset($result);

        return $result;
    }

    /**
     * Lists all PropertyTypes in QBank.
     *
     * @param null        $systemName  Returns the specified propertytype
     * @param CachePolicy $cachePolicy a custom cache policy used for this request only
     *
     * @return PropertyType[]|PropertyType|null
     */
    public function listPropertyTypes($systemName = null, CachePolicy $cachePolicy = null)
    {
        $parameters = [
            'query' => [],
            'headers' => [],
        ];

        $result = $this->get('v1/propertysets/propertytypes', $parameters, $cachePolicy);
        foreach ($result as &$entry) {
            $entry = new PropertyType($entry);
        }
        unset($entry);
        reset($result);

        if (null !== $systemName) {
            foreach ($result as $entry) {
                if ($entry->getSystemName() === $systemName) {
                    return $entry;
                }
            }

            return null;
        }

        return $result;
    }

    /**
     * Update existing PropertyType
     *
     * @param string $systemName System name of the PropertyType to update
     * @param array $definition Options to update the PropertyType with
     * @param string $name PropertyType name
     * @param string $description PropertyType description
     * @param bool $deleted Determines if the PropertyType should be deleted or not
     * @param CachePolicy $cachePolicy A custom cache policy used for this request only
     *
     * @return PropertyType
     */
    public function updatePropertyType($systemName, $definition = null, $name = null, $description = null, $deleted = null, CachePolicy $cachePolicy = null)
    {
        $parameters = [
            'query' => [],
            'body' => json_encode(['definition' => $definition, 'name' => $name, 'description' => $description, 'deleted' => $deleted], JSON_UNESCAPED_UNICODE),
            'headers' => [],
        ];

        $result = $this->put('v1/propertysets/propertytypes/' . $systemName, $parameters, $cachePolicy);
        $result = new PropertyType($result);

        return $result;
    }

    /**
     * Append options to an existing List or Dropdown PropertyType
     *
     * @param string $systemName System name of the PropertyType to update
     * @param bool $sort Determines if the new list of options should be sorted alphabetically
     * @param string[] $options Options to add to the PropertyType definition
     * @param CachePolicy $cachePolicy A custom cache policy used for this request only
     *
     * @return PropertyType
     */
    public function addPropertyTypeOptions($systemName, $sort = false, $options = [], CachePolicy $cachePolicy = null)
    {
        $parameters = [
            'query' => ['sort' => $sort],
            'body' => json_encode($options),
            'headers' => [],
        ];

        $result = $this->put('v1/propertysets/propertytypes/' . $systemName . '/options', $parameters, $cachePolicy);
        $result = new PropertyType($result);

        return $result;
    }
}
