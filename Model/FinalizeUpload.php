<?php

declare(strict_types=1);

namespace QBNK\QBank\API\Model;

class FinalizeUpload implements \JsonSerializable
{
    /**
     * Required if the file was uploaded in multiple chunks. The UploadID is returned from the generateSignedUploadUrls endpoint. 
     */
    protected ?string $uploadId;

    /**
     *  The File ID specified (or returned if one was not provided) by the generateSignedUploadUrls endpoint.
     */
    protected string $fileId;

    /**
     * The fileName that will be set on the created Media in QBank.
     */
    protected string $fileName;

    /**
     * The Category ID that will created media will be added to in QBank.
     */
    protected int $categoryId;

    /**
     * The title of the created media. $fileName will be used if not provided.
     */
    protected ?string $title;

    /**
     * If file was uploaded in multiple chunks, this parameter is required. Example:
     * [["PartNumber" => 1, "ETag" => "123451234dsadas"], ["PartNumber" => 2, "ETag" => "lkfdsakfndsa3123"]]
     */
    protected ?array $uploadParts;

    /**
     * @var int|null The user id of the user that uploaded the file (if other than the current user).
     */
    protected ?int $uploadedBy = null;

    public function __construct($parameters = [])
    {
        if (isset($parameters['uploadId'])) {
            $this->setUploadId($parameters['uploadId']);
        }
        if (isset($parameters['fileId'])) {
            $this->setFileId($parameters['fileId']);
        }
        if (isset($parameters['fileName'])) {
            $this->setFileName($parameters['fileName']);
        }
        if (isset($parameters['categoryId'])) {
            $this->setCategoryId($parameters['categoryId']);
        }
        if (isset($parameters['title'])) {
            $this->setTitle($parameters['title']);
        }
        if (isset($parameters['uploadParts'])) {
            $this->setUploadParts($parameters['uploadParts']);
        }
        if (isset($parameters['uploadedBy'])) {
            $this->setUploadedBy($parameters['uploadedBy']);
        }
    }

    /**
     * Get the value of uploadId
     */
    public function getUploadId(): ?string
    {
        return $this->uploadId;
    }

    /**
     * Set the value of uploadId
     */
    public function setUploadId(?string $uploadId): self
    {
        $this->uploadId = $uploadId;

        return $this;
    }

    /**
     * Get the value of fileId
     */
    public function getFileId(): string
    {
        return $this->fileId;
    }

    /**
     * Set the value of fileId
     */
    public function setFileId(string $fileId): self
    {
        $this->fileId = $fileId;

        return $this;
    }

    /**
     * Get the value of fileName
     */
    public function getFileName(): string
    {
        return $this->fileName;
    }

    /**
     * Set the value of fileName
     */
    public function setFileName(string $fileName): self
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * Get the value of categoryId
     */
    public function getCategoryId(): int
    {
        return $this->categoryId;
    }

    /**
     * Set the value of categoryId
     */
    public function setCategoryId(int $categoryId): self
    {
        $this->categoryId = $categoryId;

        return $this;
    }

    /**
     * Get the value of title
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * Set the value of title
     */
    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get the value of uploadParts
     */
    public function getUploadParts(): ?array
    {
        return $this->uploadParts;
    }

    /**
     * Set the value of uploadParts
     */
    public function setUploadParts(?array $uploadParts): self
    {
        $this->uploadParts = $uploadParts;

        return $this;
    }

    public function getUploadedBy(): ?int
    {
        return $this->uploadedBy;
    }

    public function setUploadedBy(?int $uploadedBy): self
    {
        $this->uploadedBy = $uploadedBy;

        return $this;
    }

    public function jsonSerialize()
    {
        $json = [];

        if (null !== $this->fileId) {
            $json['fileId'] = $this->fileId;
        }
        if (null !== $this->fileName) {
            $json['fileName'] = $this->fileName;
        }
        if (null !== $this->categoryId) {
            $json['categoryId'] = $this->categoryId;
        }
        if (null !== $this->title) {
            $json['title'] = $this->title;
        }
        if (null !== $this->uploadParts) {
            $json['uploadParts'] = $this->uploadParts;
        }
        if (null !== $this->uploadId) {
            $json['uploadId'] = $this->uploadId;
        }
        if (null !== $this->uploadedBy) {
            $json['uploadedBy'] = $this->uploadedBy;
        }


        return $json;
    }
}
