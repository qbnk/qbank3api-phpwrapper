<?php

declare(strict_types=1);

namespace QBNK\QBank\API\Model;

use DateTime;
use JsonSerializable;
use stdClass;

class SignedUploadUrlsResponse implements JsonSerializable
{
    /**
     * If chunks > 1 was provided, a upload ID will be returned that should be used to finalize the upload which effectively will combine the chunks into a single file again.
     */
    protected ?string $uploadId;

    /**
     *  Id of the file being uploaded. Should be referenced again when finalizing the upload. If no fileId parameter was provided, a fileId will be generated and returned.
     */
    protected ?string $fileId;

    /**
     * @var string[] $urls - The signed URLs that can be used for uploading a file. Number of URLs equals to the $chunks parameter provided.
     */
    protected array $urls;

    /**
     *  {@type datetime}Minimum date in this range, leave empty for none.
     */
    protected ?DateTime $expiresAt;

    public function __construct($parameters = [])
    {
        if (isset($parameters['uploadId'])) {
            $this->setUploadId($parameters['uploadId']);
        }
        if (isset($parameters['fileId'])) {
            $this->setFileId($parameters['fileId']);
        }
        if (isset($parameters['urls'])) {
            $this->setUrls($parameters['urls']);
        }
        if (isset($parameters['expiresAt'])) {
            $this->setExpiresAt($parameters['expiresAt']);
        }
    }

    /**
     * Get the value of uploadId
     */
    public function getUploadId(): ?string
    {
        return $this->uploadId;
    }

    /**
     * Set the value of uploadId
     */
    public function setUploadId(?string $uploadId): self
    {
        $this->uploadId = $uploadId;

        return $this;
    }

    /**
     * Get the value of fileId
     */
    public function getFileId(): ?string
    {
        return $this->fileId;
    }

    /**
     * Set the value of fileId
     */
    public function setFileId(?string $fileId): self
    {
        $this->fileId = $fileId;

        return $this;
    }

    /**
     * Get the value of urls
     */
    public function getUrls(): array
    {
        return $this->urls;
    }

    /**
     * Set the value of urls
     */
    public function setUrls(array $urls): self
    {
        if (!empty($urls) && is_array($urls[0])) {
            $urls = $urls[0];
        }
        $this->urls = $urls;

        return $this;
    }

    /**
     * Get {@type datetime}Minimum date in this range, leave empty for none.
     */
    public function getExpiresAt(): ?DateTime
    {
        return $this->expiresAt;
    }

    /**
     * Set {@type datetime}Minimum date in this range, leave empty for none.
     */
    public function setExpiresAt(string $expiresAt): self
    {
        if ($expiresAt instanceof DateTime) {
            $this->expiresAt = $expiresAt;
        } else {
            try {
                $this->expiresAt = new DateTime($expiresAt);
            } catch (\Exception $e) {
                $this->expiresAt = null;
            }
        }

        return $this;
    }

    public function jsonSerialize(): mixed
    {
        $data = [];
        $data['expiresAt'] = $this->expiresAt ?? null;
        $data['urls'] = $this->urls ?? null;
        $data['fileId'] = $this->fileId ?? null;
        $data['uploadId'] = $this->uploadId ?? null;
        return $data;
    }

}
