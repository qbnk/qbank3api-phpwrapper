<?php

namespace QBNK\QBank\API\Model;


class ClientId implements \JsonSerializable
{
    /** @var string The ClientId Id. */
    protected $id;
    /** @var string The redirect URI. */
    protected $redirectUri;
    /** @var string Notes regarding this client Id. */
    protected $notes;
    /** @var string The secret of this client Id. */
    protected $secret;

    /**
     * Constructs a ClientId.
     *
     * @param array $parameters An array of parameters to initialize the {@link ClientId} with.
     *                          - <b>id</b> - The ClientId Id.
     *                          - <b>redirectUri</b> - The redirect URI.
     *                          - <b>notes</b> - Notes regarding this client Id.
     *                          - <b>secret</b> - The secret of this client Id.
     */
    public function __construct($parameters = [])
    {
        if (isset($parameters['id'])) {
            $this->setId($parameters['id']);
        }

        if (isset($parameters['redirectUri'])) {
            $this->setRedirectUri($parameters['redirectUri']);
        }

        if (isset($parameters['notes'])) {
            $this->setNotes($parameters['notes']);
        }

        if (isset($parameters['secret'])) {
            $this->setSecret($parameters['secret']);

        }
    }

    /**
     * Gets all data that should be available in a json representation.
     *
     * @return array an associative array of the available variables
     */
    public function jsonSerialize(): mixed
    {
        $json = [];

        if (null !== $this->id) {
            $json['id'] = $this->id;
        }
        if (null !== $this->redirectUri) {
            $json['redirectUri'] = $this->redirectUri;
        }
        if (null !== $this->notes) {
            $json['notes'] = $this->notes;
        }
        if (null !== $this->secret) {
            $json['secret'] = $this->secret;
        }

        return $json;
    }

    /**
     * @param string $id
     * @return ClientId
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $redirectUri
     * @return ClientId
     */
    public function setRedirectUri($redirectUri)
    {
        $this->redirectUri = $redirectUri;
        return $this;
    }

    /**
     * @return string
     */
    public function getRedirectUri()
    {
        return $this->redirectUri;
    }

    /**
     * @param string $notes
     * @return ClientId
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
        return $this;
    }

    /**
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * @param string $secret
     * @return ClientId
     */
    public function setSecret($secret)
    {
        $this->secret = $secret;
        return $this;
    }

    /**
     * @return string
     */
    public function getSecret()
    {
        return $this->secret;
    }
}