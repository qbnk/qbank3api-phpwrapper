FROM php:8.1-fpm-alpine

COPY --from=composer:2 /usr/bin/composer /usr/local/bin/composer

RUN apk add --no-cache libxml2-dev

RUN docker-php-ext-install \
    dom \
    xml \
